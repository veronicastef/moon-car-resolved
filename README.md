# Moon Car

####Author

* Veronica Stefani Lopez Salvatierra

####Requeriments

* Java JDK 1.8

####How to start?

1. Go to directory :

       cd src/

2. Compile executing  the command :

       javac *.java

3. Run using the command :

       java Main

####For the first Ship

* First introduce the plateau's coordinates.
* Second introduce the ship's coordinates.
* Third introduce the commands for move the ship. 

####For the second Ship

* Introduce the coordinates of the ship.
* Introduce the commands for move the ship.  
 
###General

1. Coordinates of the plateau they are only necessary for the first ship.
      
   - Only accepts numeric values

2. Coordinates of the Ship they are necessary 3 values x,y and orientation.
      
   - The first two digits only accept numerical values ​​and the third only the letters __"N W S E"__ (upper case letters and lower case letters)

3. Commands to move the ship
      
   - Only accepts the letters __"M L R"__ (upper case letters and lower case letters)
   
4. If the ship is about to leave the plateau it will dont move 

#####EXAMPLE
* __Test Input__
 
    5 5    
    1 2 N  
    LMLMLMLMM 
    3 3 E   
    MMRMMRMRRM  
  
 * __Test Output__

    1 3 N  
    5 1 E   
    ==========
 