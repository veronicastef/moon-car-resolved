package Utilitarie;

import Actions.Action;
import Actions.LeftAction;
import Actions.MoveAction;
import Actions.rightAction;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by veronica on 08/12/2017.
 */
public class Utilitaries {

    public static List<String> orientations = Arrays.asList("N", "W", "S", "E");

    public static String getOrientation(int index) {
        return orientations.get(index);
    }

    public static int getIndexOrientation(String text) {
        return orientations.indexOf(text);
    }

    public static String noSpaces(String text) {
        java.util.StringTokenizer tokens = new java.util.StringTokenizer(text);
        StringBuilder buff = new StringBuilder();
        while (tokens.hasMoreTokens()) {
            buff.append(" ").append(tokens.nextToken());
        }
        return buff.toString().trim();
    }

    public static int convertStringToInt(String text) {
        return Integer.parseInt(text);
    }

    public static boolean isValid(String text, String textToFind) {
        return text.indexOf(textToFind) != -1;
    }

    public static List<Action> getActions(String commands) {
        List<Action> actions = new ArrayList<Action>(0);
        for (int x = 0; x < commands.length(); x++) {
            switch (commands.charAt(x)) {
                case 'L':
                    actions.add(new LeftAction());
                    break;
                case 'M':
                    actions.add(new MoveAction());
                    break;
                case 'R':
                    actions.add(new rightAction());
                    break;
            }
        }
        return actions;
    }

    public static boolean isNumeric(String text){
        try {
            Integer.parseInt(text);
            return true;
        } catch (NumberFormatException nfe){
            return false;
        }
    }
}
