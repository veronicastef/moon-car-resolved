package Actions;


import Entities.Ship;

/**
 * Created by veronica on 08/12/2017.
 */
public class rightAction implements Action {

    @Override
    public Ship execute(Ship ship) {
        ship.orientation--;
        if (ship.orientation < 0) {
            ship.orientation = 3;
        }
        return ship;
    }
}
