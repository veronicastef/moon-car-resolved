package Actions;


import Entities.Ship;

/**
 * Created by veronica on 08/12/2017.
 */
public class LeftAction implements Action {

    @Override
    public Ship execute(Ship ship) {
        ship.orientation++;
        if (ship.orientation > 3) {
            ship.orientation = 0;
        }
        return ship;
    }
}
