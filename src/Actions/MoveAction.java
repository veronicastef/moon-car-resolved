package Actions;


import Entities.Ship;
import Utilitarie.Coordinates;

/**
 * Created by veronica on 08/12/2017.
 */
public class MoveAction implements Action {

    @Override
    public Ship execute(Ship ship) {
        Ship shipTemporary = new Ship(new Coordinates(ship.coordinates.X, ship.coordinates.Y), ship.orientation);
        switch (shipTemporary.orientation) {
            case 0:
                shipTemporary.coordinates.Y++;
                break;
            case 1:
                shipTemporary.coordinates.X--;
                break;
            case 2:
                shipTemporary.coordinates.Y--;
                break;
            case 3:
                shipTemporary.coordinates.X++;
                break;
        }
        return shipTemporary;
    }
}
