package Actions;

import Entities.Ship;

/**
 * Created by veronica on 08/12/2017.
 */
public interface Action {

    Ship execute(Ship ship);
}
