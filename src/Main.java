import Actions.Action;

import Entities.Plateau;
import Entities.Ship;
import Utilitarie.Coordinates;
import Utilitarie.Utilitaries;


import java.util.List;
import java.util.Scanner;

/**
 * Created by veronica on 08/12/2017.
 */
public class Main {
    public static void main(String[] args) {
        String enteredPlateau;
        Scanner enteredScanner = new Scanner(System.in);

        enteredPlateau = enteredScanner.nextLine();

        enteredPlateau = Utilitaries.noSpaces(enteredPlateau).toUpperCase();
        String[] coordinatesPlateau = enteredPlateau.split(" ");
        if (!Plateau.isValidCoordinate(coordinatesPlateau)) {
            System.out.println("Please Introduce valid coordinates");
            return;
        }

        String enteredShipOne = enteredScanner.nextLine();
        enteredShipOne = Utilitaries.noSpaces(enteredShipOne).toUpperCase();
        String[] coordinatesShipOne = enteredShipOne.split(" ");
        if (!Ship.isValidCoordinate(coordinatesShipOne)) {
            System.out.println("Please Introduce valid coordinates");
            return;
        }
        String commandssShipOne = enteredScanner.nextLine();

        String enteredShipTwo = enteredScanner.nextLine();
        enteredShipTwo = Utilitaries.noSpaces(enteredShipTwo).toUpperCase();
        String[] coordinatesShipTwo = enteredShipTwo.split(" ");
        if (!Ship.isValidCoordinate(coordinatesShipTwo)) {
            System.out.println("Please Introduce valid coordinates");
            return;
        }
        String commandssShipTwo = enteredScanner.nextLine();

        Plateau plateau = new Plateau(new Coordinates(Utilitaries.convertStringToInt(coordinatesPlateau[0]), Utilitaries.convertStringToInt(coordinatesPlateau[1])));
        Ship shipOne = new Ship(new Coordinates(Utilitaries.convertStringToInt(coordinatesShipOne[0]), Utilitaries.convertStringToInt(coordinatesShipOne[1])), Utilitaries.getIndexOrientation(coordinatesShipOne[2]));
        Ship shipTwo = new Ship(new Coordinates(Utilitaries.convertStringToInt(coordinatesShipTwo[0]), Utilitaries.convertStringToInt(coordinatesShipTwo[1])), Utilitaries.getIndexOrientation(coordinatesShipTwo[2]));
        List<Action> actionsShipOne = Utilitaries.getActions(commandssShipOne);
        List<Action> actionsShipTwo = Utilitaries.getActions(commandssShipTwo);

        for (Action action : actionsShipOne) {
            Ship temporaryShip = action.execute(shipOne);
            if (plateau.isOnThePlateau(temporaryShip.coordinates)) {
                shipOne.orientation = temporaryShip.orientation;
                shipOne.coordinates.Y = temporaryShip.coordinates.Y;
                shipOne.coordinates.X = temporaryShip.coordinates.X;
            }
        }

        for (Action action : actionsShipTwo) {
            Ship temporaryShip = action.execute(shipTwo);
            if (plateau.isOnThePlateau(temporaryShip.coordinates)) {
                shipTwo.orientation = temporaryShip.orientation;
                shipTwo.coordinates.Y = temporaryShip.coordinates.Y;
                shipTwo.coordinates.X = temporaryShip.coordinates.X;
            }
        }
        System.out.println(shipOne.coordinates.X + " " + shipOne.coordinates.Y + " " + Utilitaries.getOrientation(shipOne.orientation));
        System.out.println(shipTwo.coordinates.X + " " + shipTwo.coordinates.Y + " " + Utilitaries.getOrientation(shipTwo.orientation));
        System.out.println("==========");
    }
}
