package Entities;

import Utilitarie.Coordinates;
import Utilitarie.Utilitaries;

/**
 * Created by veronica on 10/12/2017.
 */
public class Ship {

    public int orientation;
    public Coordinates coordinates;

    public Ship(Coordinates coordinates, int orientation) {
        this.coordinates = coordinates;
        this.orientation = orientation;
    }

    public static boolean isValidCoordinate(String[] coordinatesPlateau) {
        return coordinatesPlateau.length == 3 && !(!Utilitaries.isNumeric(coordinatesPlateau[0])
                || !Utilitaries.isNumeric(coordinatesPlateau[1])
                || !Utilitaries.isValid("NWSE", coordinatesPlateau[2]));
    }
}
