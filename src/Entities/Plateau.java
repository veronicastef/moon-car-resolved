package Entities;

import Utilitarie.Coordinates;
import Utilitarie.Utilitaries;

/**
 * Created by veronica on 10/12/2017.
 */
public class Plateau {
    Coordinates coordinatesLeft;
    Coordinates coordinatesRight = new Coordinates(0, 0);

    public Plateau(Coordinates coordinatesLeft) {
        this.coordinatesLeft = coordinatesLeft;
    }

    public boolean isOnThePlateau(Coordinates coordinatesShip) {
        if ((coordinatesShip.X >= this.coordinatesRight.X) && (coordinatesShip.X <= this.coordinatesLeft.X) &&
                (coordinatesShip.Y >= this.coordinatesRight.Y) && (coordinatesShip.Y <= this.coordinatesLeft.Y)) {
            return true;
        }
        return false;
    }

    public static boolean isValidCoordinate(String[] coordinatesPlateau) {
        if (coordinatesPlateau.length != 2) {
            return false;
        }
        for (String coordinatePlateau : coordinatesPlateau) {
            if (!Utilitaries.isNumeric(coordinatePlateau)) {
                return false;
            }
        }
        return true;
    }
}
